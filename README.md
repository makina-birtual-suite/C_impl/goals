# Goals


## Disclaimers
- This subgroup focuses on providing a C only implementation of all the essential tools of the Makina Birtual MKNASM standard.
- No code in this subgroup may be implemented in any other language than C (and it's build tools) or MKNASM.
- Any code in here is not held up to any standard other than implementing the MKNASM standard. The standard is defined on the Rust implementation, also known as the official one.
- Any contributors are welcome. 
